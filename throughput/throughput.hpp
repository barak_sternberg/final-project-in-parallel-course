#pragma once

#include <cassert>

#include "common/distribute.hpp"

namespace throughput
{
template<class Manager, class UpdatePolicy>
class worker
{
private:
	using observer_type = typename Manager::observer_type;
	using task_type = typename Manager::queue_type::element;
	observer_type tasks_;
	UpdatePolicy update_task_;
	size_t operation_count_;

public:
	worker(observer_type&& tasks, UpdatePolicy&& update_task, size_t operation_count):
		tasks_(std::move(tasks)), update_task_(std::move(update_task)), operation_count_(operation_count)
	{
		assert(operation_count_ % 2 == 0);
	}

	void operator()()
	{
		// At this point the queue is pretty full, and wont ever be empty
		for(size_t i = 0; i<operation_count_/2; i++)
		{
			auto task = tasks_.top_pop();
			update_task_(task);
			tasks_.push(std::move(task));
		}
	}

};

template<class Types, class FillPolicy, class UpdatePolicy>
class manager
{
private:
	using queue_type = typename Types::queue_type;
	using task_type = typename queue_type::element;
	using update_policy = UpdatePolicy;
	using worker_type = worker<manager, update_policy>;
	using observer_type = typename Types::observer_type;

	queue_type tasks_;
	FillPolicy fill_policy_;
	update_policy update_policy_;
	double throughput_;

	friend worker_type;

	auto make_worker(size_t idx, size_t operation_count)
	{
		return worker_type(observer_type(tasks_, idx), update_policy_.duplicate(), operation_count);
	}

	void fill_queue(size_t n)
	{
		for(size_t i=0; i<n; i++)
		{
			tasks_.push(fill_policy_());
		}
	}

public:
	/*
	 * @param graph
	 * @param source
	 * @param thread count - allowed to be 0, but notice that the queue must not know that.
	 * 0 is a sentinel value for own thread execution.
	 * @param args - arguments to be passed to the queue.
	 */
	template<class... Args>
	manager(size_t thread_count, size_t n0, size_t operation_count,
			FillPolicy&& fill_policy, UpdatePolicy&& update_policy,
			Args&&... args):
	  tasks_(std::max(thread_count, size_t{1}), n0+operation_count, std::forward<Args>(args)...),
		fill_policy_(std::move(fill_policy)), update_policy_(std::move(update_policy)),
		throughput_(-1)
	{
		fill_queue(n0);
		auto actual_count = std::max(thread_count, size_t{1});
		auto each_thread = operation_count/actual_count;
		auto factory = [&](size_t i)
		{
			return make_worker(i, each_thread);
		};
		auto duration = common::distribute(factory, thread_count);
		throughput_ = operation_count/duration;
	}

	auto throughput() const
	{
		return throughput_;
	}
};

// Extra arguments are passed to queue constructor
template<class Types, class FillPolicy, class UpdatePolicy, class... Args>
auto calculate(
		FillPolicy fill,
		UpdatePolicy update,
		size_t n0, size_t operation_count,
		size_t thread_count, Args&&... args)
{
	manager<Types, FillPolicy, UpdatePolicy> manager(
			thread_count, n0, operation_count,
			std::move(fill), std::move(update),
			std::forward<Args>(args)...);
	return manager.throughput();
}

}


