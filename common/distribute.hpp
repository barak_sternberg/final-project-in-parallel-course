#pragma once

#include <thread>
#include <vector>
#include <chrono>

namespace common
{

template<class Factory>
double distribute(Factory& factory, size_t thread_count)
{
	std::chrono::system_clock::time_point start, end;
	if(thread_count == 0)
	{
			auto worker = factory(0);
			start = std::chrono::system_clock::now();
			worker();
			end = std::chrono::system_clock::now();
	}
	else
	{
		std::vector<std::thread> threads(thread_count);
		int i = 0;
		start = std::chrono::system_clock::now();
		for(auto& t: threads)
		{
			t = std::thread(factory(i++));
		}
		for(auto& t: threads)
		{
			t.join();
		}
		end = std::chrono::system_clock::now();
	}
	return std::chrono::duration<double>(end - start).count();
}

}
