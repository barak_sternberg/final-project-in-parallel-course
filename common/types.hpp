#pragma once

#include <tuple>
#include <utility>

namespace common
{

template<class Type>
struct type_name;

template<class Type>
struct type_tag
{
	using type = Type;
};

template<class Type>
struct wrap_tag
{
	using type = type_tag<Type>;
};

namespace detail
{

template<class Types, size_t... I, class F>
void for_each_impl(Types&& t, std::index_sequence<I...>, F&& f)
{
	int a[] = {(f(std::get<I>(t)), 0)...};
	(void)a;
}

}

template<class Types, class F>
void for_each(Types&& t, F&& f)
{
	detail::for_each_impl(
			std::forward<Types>(t),
			std::make_index_sequence<std::tuple_size<Types>::value>{},
			std::forward<F>(f));
}

template<template<class> class F, class Seq>
struct transform;

template<template<class> class F, template<class...> class Seq, class... Args>
struct transform<F, Seq<Args...>>
{
	using type = Seq<typename F<Args>::type...>;
};

}
