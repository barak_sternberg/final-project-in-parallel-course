#pragma once

#include <cstdint>

namespace common
{
#ifdef CACHE_LINE_SIZE
constexpr std::size_t cache_line_size = CACHE_LINE_SIZE;
#undef CACHE_LINE_SIZE
#else
constexpr std::size_t cache_line_size = 64; // Assume a 64 byte cache line
#endif
}
