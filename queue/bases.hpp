#pragma once

#include <vector>
#include <mutex>
#include <memory>

#include "common/random_maker.hpp"
#include "common/cache_line.hpp"
#include "common/spinlock.hpp"
#include "common/reference_counter.hpp"

namespace queue
{
namespace bases
{

using namespace common;

template<class QueueType, class MetaData>
struct multi_queue
{
	using queue_type = QueueType;
	using element = typename queue_type::element;
private:
	using priority_type = decltype(std::declval<element>().priority());

	struct alignas(cache_line_size) control_type
	{
		MetaData metadata;
		spinlock mutex;

		inline auto get_lock()
		{
			return std::unique_lock<spinlock>(mutex, std::try_to_lock);
		}

		control_type(){}
	};

	static_assert(alignof(queue_type) == cache_line_size,
			"Queue type must be aligned to cache");
	// Members for multithreaded access
	reference_counter<> non_empty_count{0};
	alignas(cache_line_size) std::vector<queue_type> queues;
	alignas(cache_line_size) std::unique_ptr<control_type[]> control;
	// Use std::unique_ptr<T[]> instead vector for unmoveable types

	// Members for single threaded/const access
	const size_t factor_;

	random_maker prng;

public:	
	/*
	 * *_impl methods should only be called from:
	 * - Observer under a lock for control[index].get_lock()
	 * - Queue when thread safety is not required.
	 */
	auto top_pop_impl(size_t index)
	{
		auto& queue = queues[index];
		auto was_empty = queue.empty();
		auto task = queue.top_pop();
		auto is_empty = queue.empty();
		metadata(index).notify_pop(queue, task);
		if(is_empty && !was_empty)
		{
			non_empty_count.decrement();
		}
		return std::move(task);
	}

	template<class Element>
	void push_impl(size_t index, Element&& task)
	{
		auto& queue = queues[index];
		auto was_empty = queue.empty();
		queue.push(task);
		metadata(index).notify_push(queue, task);
		if(was_empty)
		{
			non_empty_count.increment();
		}
	}

	inline auto& metadata(size_t index) noexcept
	{
		return control[index].metadata;
	}

	inline auto& metadata(size_t index) const noexcept
	{
		return control[index].metadata;
	}

	inline bool empty() const noexcept
	{
		return non_empty_count.empty();
	}

	inline auto get_lock(int index) noexcept
	{
		return control[index].get_lock();
	}

	inline auto subqueue_empty(int index) noexcept
	{
		return queues[index].empty();
	}

	template<class Element>
	inline void push(Element&& task)
	{
		push_impl(prng(), std::forward<Element>(task));
	}

	inline auto queue_count() const noexcept
	{
		return queues.size();
	}

	inline auto factor() const noexcept
	{
		return factor_;
	}

	multi_queue(size_t thread_count, size_t task_count, size_t factor=2):
		queues(thread_count*factor),
		control(std::make_unique<control_type[]>(queue_count())),
		factor_(factor),
		prng(0, queue_count() - 1)
	{
		for(auto& queue: queues)
		{
			queue.reserve(task_count);
		}
	}
};

template<class MultiQueueType, class PushPolicy, class PopPolicy>
struct observer
{
private:
	using multi_queue_type = MultiQueueType;
	using element = typename multi_queue_type::element;
	multi_queue_type& mq;
	random_maker any_queue_;
	random_maker alloted_queue_;
	PushPolicy push_policy;
	PopPolicy pop_policy;

public:

	static constexpr int NOT_FOUND = -1;

	inline auto& metadata(int index) const noexcept
	{
		return mq.metadata(index);
	}

	auto any() noexcept
	{
		return any_queue_();
	}

	auto alloted() noexcept
	{
		return alloted_queue_();
	}

	observer(multi_queue_type& mq_, size_t tid_):
		mq(mq_),
		any_queue_(0, mq.queue_count() - 1),
		alloted_queue_(tid_ * mq.factor(), (tid_ + 1) * mq.factor() - 1)
	{}

	template<class Element>
	void push(Element&& e)
	{
		while(true)
		{
			auto idx = push_policy(*this);
			if(idx == NOT_FOUND)
			{
				// Passable fallback.
				idx = any();
			}
			auto l = mq.get_lock(idx);
			if(l)
			{
				mq.push_impl(idx, std::forward<Element>(e));
				return;
			}
		}
	}

	element top_pop()
	{
		do
		{
			auto idx = pop_policy(*this);
			if(idx == NOT_FOUND)
			{
				continue;
			}
			auto lock = mq.get_lock(idx);
			if(lock)
			{
				if(!mq.subqueue_empty(idx))
				{
					return mq.top_pop_impl(idx);
				}
			}
		}
		while(!mq.empty());
		return {};
	}

	void dispose(element&&) {}
};

}
}
