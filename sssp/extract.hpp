#pragma once

#include <fstream>
#include <vector>
#include <utility>
#include <limits>
#include <iostream>

namespace sssp
{

template<class Graph, class F>
auto read_snap_unweighted(std::istream& in, F&& weights_generator)
{
	constexpr char COMMENT = '#';
	std::vector<std::pair<int, int>> edges;
	int max_node = 0;
	while(in && in.peek() == COMMENT)
	{ // discard comment lines
		in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	while(in)
	{
		std::pair<int, int> p;
		in >> p.first >> p.second;
		if(!in)
		{
			break;
		}
		max_node = std::max(max_node, std::max(p.first, p.second));
		edges.push_back(std::move(p));
	}
	Graph g(max_node+1);
	for(const auto& e: edges)
	{
		add_edge(e.first, e.second, weights_generator(), g);
	}
	return g;
}
}
