#pragma once

#include <type_traits>
#include <boost/graph/adjacency_list.hpp>
#include <cstdint>

#include "common/task.hpp"

namespace sssp
{

namespace detail
{
	using namespace boost;
	using graph_type = adjacency_list<vecS, vecS, directedS,
				no_property, common::task::member_type>;
}
using detail::graph_type;

static_assert(std::is_convertible<graph_type::vertex_descriptor, int>::value,
			"Graph vertex_descriptor must be convertible to an int");

}
