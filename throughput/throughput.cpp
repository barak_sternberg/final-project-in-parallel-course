#include <iostream>
#include <cstring>

#include "throughput.hpp"
#include "queue/multi_queue.hpp"
#include "common/types.hpp"
#include "common/policies.hpp"

int main(int argc, char* argv[])
{
	using namespace throughput;
	int thread_count=2;
	int n0 = 1'000'000;
	int op_count = 40'000'000;
	if(argc > 1 && !std::strcmp(argv[1], "-h"))
	{
		std::cerr << "Usage: " << argv[0] << " [thread-count=" << thread_count << "] ";
		std::cerr << "[op-count=" << op_count << "] [n0=" << n0 << "]" << std::endl;
		return 0;
	}
	if(argc > 1)
	{
		thread_count = std::atoi(argv[1]);
	}
	if(argc > 2)
	{
		op_count = std::atoi(argv[2]);
	}
	if(argc > 3)
	{
		n0 = std::atoi(argv[3]);
	}
	common::uniform_policy policy(0, 100'000'000);
	std::cout << "Starting computation: " << op_count << " ops" << std::endl;
	common::for_each(queue::all{}, [&](auto e)
	{
		using types = typename decltype(e)::type;
		auto res = throughput::calculate<types>(policy.duplicate(), policy.duplicate(), n0, op_count, thread_count);
		std::cout << common::type_name<types>::value << '\t' << res << std::endl;
	});
	return 0;
}
