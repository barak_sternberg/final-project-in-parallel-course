#pragma once

#include <cstdint>
#include <iostream>
#include <limits>

namespace common
{

class task
{
public:
	using member_type = std::int32_t;
	static constexpr auto max_priority = std::numeric_limits<member_type>::max();
private:
	using raw_type = std::int_fast64_t; // word size for most PCs
	static constexpr std::size_t high_shift = 32;
	static constexpr raw_type low_mask = 0xFFFFFFFF;
	static constexpr raw_type EMPTY = -1;
	raw_type storage;

	static inline constexpr raw_type combine(member_type priority, member_type value)
	{
		return (static_cast<raw_type>(priority) << high_shift) | static_cast<raw_type>(value);
	}

public:
	task(member_type priority, member_type value):
		storage(combine(priority, value)) {}

	template<class Integral1, class Integral2>
	task(Integral1 priority, Integral2 value):
		task(static_cast<member_type>(priority), static_cast<member_type>(value)) {}
	// Might introduce some errors

	task(): storage(EMPTY) {}

	task(const task&) = default;
	task& operator=(const task&) = default;
	task(task&&) = default;
	task& operator=(task&&) = default;

	inline member_type priority() const
	{
		return storage >> high_shift;
	}

	inline member_type value() const
	{
		return storage & low_mask;
	}

	inline operator bool() const
	{
		return storage != EMPTY;
	}

	inline void clear()
	{
		storage = EMPTY;
	}

	inline bool operator<(const task& rhs) const
	{
		return storage < rhs.storage; // lex compare (priority, value)
	}

	inline bool operator>(const task& rhs) const
	{
		return rhs < *this;
	}

	inline bool operator==(const task& rhs) const
	{
		return storage == rhs.storage;
	}

	inline bool operator!=(const task& rhs) const
	{
		return !(*this == rhs);
	}
};

std::ostream& operator<<(std::ostream& out, const task& t)
{
	out << "task{" << t.priority() << ',' << t.value() << '}';
	return out;
}

}

