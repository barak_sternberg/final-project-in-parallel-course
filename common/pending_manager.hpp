#pragma once

#include "reference_counter.hpp"

namespace common
{

struct pending_manager
{
	using counter_type = reference_counter<>;

private:
	bool pending=false;
	counter_type& working_counter;

public:
	pending_manager(counter_type& counter): working_counter(counter)
	{
		working_counter.increment();
	}

	bool pend()
	{
		bool empty;
		// Mark the thread as pending
		// Returns if any thread is working.
		if(!pending) // first time got an empty queue
		{
			empty = working_counter.decrement();
			// mark self as pending
			pending = true;
		}
		else
		{
			empty = working_counter.empty();
			// read current work counter
		}
		return !empty;
		// wait only of there are other waiting threads
	}

	void resume()
	{
		// Mark the thread as a working thread.
		// If the thread wasn't pending, this is a no-op
		if(!pending)
		{
			return;
		}
		pending = false;
		working_counter.increment();
	}
};
}
