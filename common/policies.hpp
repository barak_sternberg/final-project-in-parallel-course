#pragma once

#include "task.hpp"
#include "random_maker.hpp"

namespace common
{

struct uniform_policy
{
private:
	random_maker prng;

	uniform_policy(random_maker&& prng_):
		prng(std::move(prng_)) {}

public:

	task operator()()
	{
		return {prng(), prng()};
	}

	void operator()(task& t)
	{
		t = (*this)();
	}

	uniform_policy(size_t bottom, size_t top):
		prng(bottom, top) {}

	uniform_policy duplicate() const
	{
		return {prng.duplicate()};
	}
};

}
