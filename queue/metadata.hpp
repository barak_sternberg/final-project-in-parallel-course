#pragma once

#include <atomic>
#include <cstdint>
#include <cassert>

namespace queue
{
namespace metadata
{

template<class PriorityType>
struct top
{
private:
	using priority_type = PriorityType;
	static constexpr priority_type EMPTY = -1;

	std::atomic<priority_type> data;

	template<class Queue>
	inline void store_min(const Queue& queue) noexcept
	{
		const priority_type& val =
			queue.empty() ? EMPTY : queue.top().priority();
		data.store(val, std::memory_order_release);
	}

public:
	top() noexcept : data{EMPTY}
	{
		assert(data.is_lock_free());
	}

	template<class Queue>
	inline void notify_pop(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_min(queue);
	}

	template<class Queue>
	inline void notify_push(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_min(queue);
	}

	inline auto load() const noexcept
	{
		return data.load(std::memory_order_acquire);
	}

	static inline bool is_valid(const priority_type& value) noexcept
	{
		return value != EMPTY;
	}
};

struct length
{
private:

	std::atomic<std::size_t> data;

	template<class Queue>
	inline void store_length(const Queue& queue) noexcept
	{
		data.store(queue.size(), std::memory_order_release);
	}

public:
	length() noexcept : data{0}
	{
		assert(data.is_lock_free());
	}

	template<class Queue>
	inline void notify_pop(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_length(queue);
	}

	template<class Queue>
	inline void notify_push(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_length(queue);
	}

	inline auto load() const noexcept
	{
		return data.load(std::memory_order_acquire);
	}

	static inline bool is_valid(const std::size_t& value) noexcept
	{
		return value != 0;
	}
};

struct is_empty
{
	std::atomic<bool> data;
private:

	template<class Queue>
	inline void store_valid(const Queue& queue) noexcept
	{
		data.store(!queue.empty(), std::memory_order_release);
	}
public:
	is_empty() noexcept : data{false}
	{
		assert(data.is_lock_free());
	}
	template<class Queue>
	inline void notify_pop(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_valid(queue);
	}

	template<class Queue>
	inline void notify_push(const Queue& queue, const typename Queue::element&) noexcept
	{
		store_valid(queue);
	}

	inline auto load() const noexcept
	{
		return data.load(std::memory_order_acquire);
	}

	static inline auto is_valid(const bool& v) noexcept
	{
		return v;
	}
};

struct nothing
{
	template<class Queue>
	inline void notify_pop(const Queue&, const typename Queue::element&) noexcept {}

	template<class Queue>
	inline void notify_push(const Queue&, const typename Queue::element&) noexcept{}

	inline auto load() const noexcept {}

	static inline auto is_valid(const bool&) noexcept {}
};

template<class PriorityType>
struct front_back
{
private:
	using priority_type = PriorityType;
	static constexpr priority_type EMPTY = -1;

	std::atomic<priority_type> front_;
	std::atomic<priority_type> back_;

public:

	front_back() noexcept : front_{EMPTY}, back_{EMPTY}
	{
		assert(front_.is_lock_free());
		assert(back_.is_lock_free());
	}

	template<class Queue>
	inline void notify_pop(const Queue& queue, const typename Queue::element&) noexcept
	{
		if(queue.empty())
		{
			front_.store(EMPTY, std::memory_order_release);
			back_.store(EMPTY, std::memory_order_release);
		}
		else
		{
			front_.store(queue.front(), std::memory_order_release);
		}
	}

	template<class Queue>
	inline void notify_push(const Queue& queue, const typename Queue::element&) noexcept
	{
		auto old = back_.exchange(queue.back(), std::memory_order_release);
		if(old == EMPTY)
		{
			front_.store(queue.front(), std::memory_order_release);
		}
	}

	inline auto front() const noexcept
	{
		return front_.load(std::memory_order_acquire);
	}

	inline auto back() const noexcept
	{
		return back_.load(std::memory_order_acquire);
	}

	static inline auto is_valid(const priority_type& v) noexcept
	{
		return v != EMPTY;
	}
};

template<class PriorityType>
struct approximate_max
{
private:
	using priority_type = PriorityType;
	static constexpr priority_type EMPTY = -1;

	std::atomic<priority_type> data;

public:
	approximate_max() noexcept : data{EMPTY}
	{
		assert(data.is_lock_free());
	}

	template<class Queue>
	inline void notify_pop(const Queue& queue, const typename Queue::element&) noexcept
	{
		if(queue.empty())
		{
			data.store(EMPTY, std::memory_order_release);
		}
	}

	template<class Queue>
	inline void notify_push(const Queue&, const typename Queue::element& pushed) noexcept
	{
		auto new_p = pushed.priority();
		if(new_p > data.load(std::memory_order_acquire))
		{
			data.store(new_p, std::memory_order_release);
		}
	}

	inline auto load() const noexcept
	{
		return data.load(std::memory_order_acquire);
	}

	static inline bool is_valid(const priority_type& value) noexcept
	{
		return value != EMPTY;
	}
};

}
}
