#pragma once

#include "queues.hpp"
#include "observers.hpp"
#include "common/types.hpp"
#include "common/task.hpp"
#include "lock_queue.hpp"

#define NAME_QUEUE(class_)\
namespace common\
{\
using namespace queue;\
template<>\
struct type_name<class_>\
{\
	static constexpr char const * value = #class_;\
};\
}

namespace queue
{

template<template<class, class> class Queue, template<class> class Observer>
struct make_queue
{
	using integer_type = common::task::member_type;
	using queue_type = Queue<integer_type, integer_type>;
	using observer_type = Observer<queue_type>;
};

using namespace queues;
using namespace observers;

using stupid = make_queue<no_info_queue, random>;
using classic = make_queue<min_element_queue, pop_min>;
using pop_max_length = make_queue<length_queue, pop_max>;
using non_empty = make_queue<is_empty_queue, pop_valid>;
using worst = make_queue<max_element_queue, pop_max>;
using alloted = make_queue<min_element_queue, pop_min_alloted>;
using stupid_alloted = make_queue<no_info_queue, pop_random_alloted>;
using push_min_length = make_queue<length_queue, push_min>;
using circular = make_queue<queues::circular_queue, observers::front_back>;
using lock_queue = make_queue<lock::queue, lock::observer>;
using approximate_max_element = make_queue<approximate_max_queue, push_max>;

using all_types = std::tuple<
	classic,
	non_empty, stupid,
	alloted, stupid_alloted,
	pop_max_length, push_min_length,
	circular,
	approximate_max_element,
	lock_queue, worst>;

using all = common::transform<common::wrap_tag, all_types>::type;
}

NAME_QUEUE(stupid)
NAME_QUEUE(classic)
NAME_QUEUE(pop_max_length)
NAME_QUEUE(non_empty)
NAME_QUEUE(worst)
NAME_QUEUE(alloted)
NAME_QUEUE(stupid_alloted)
NAME_QUEUE(push_min_length)
NAME_QUEUE(circular)
NAME_QUEUE(lock_queue)
NAME_QUEUE(approximate_max_element)

#undef NAME_QUEUE
