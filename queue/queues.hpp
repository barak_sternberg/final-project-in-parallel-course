#pragma once

#include "tasks_queue.hpp"
#include "bases.hpp"
#include "metadata.hpp"
#include "circular_queue.hpp"

namespace queue
{
namespace queues
{

using bases::multi_queue;
using namespace metadata;
using namespace sequential;

template<class PriorityType, class ValueType>
using no_info_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType>,
		nothing
		>;

template<class PriorityType, class ValueType>
using is_empty_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType>,
		is_empty
		>;

template<class PriorityType, class ValueType>
using min_element_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType>,
		top<PriorityType>
		>;

template<class PriorityType, class ValueType>
using max_element_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType, false>,
		top<PriorityType>
		>;

template<class PriorityType, class ValueType>
using length_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType>,
		length
		>;

template<class PriorityType, class ValueType>
using circular_queue =
	multi_queue<
		circular_queue<PriorityType, ValueType>,
		front_back<PriorityType>
		>;

template<class PriorityType, class ValueType>
using approximate_max_queue =
	multi_queue<
		tasks_queue<PriorityType, ValueType>,
		approximate_max<PriorityType>
		>;

}
}

