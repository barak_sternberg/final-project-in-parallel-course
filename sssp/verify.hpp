#pragma once

#include <limits>
#include <boost/range/iterator_range_core.hpp>
#include <iostream>

namespace sssp
{

template<class Graph, class Weights>
bool verify(const Graph& graph, const Weights& weights)
{
	using boost::make_iterator_range;
	constexpr auto max_weight = std::numeric_limits<typename Weights::value_type>::max();
	bool ok = true;
	for(const auto& node: make_iterator_range(vertices(graph)))
	{
		for(const auto& edge: make_iterator_range(out_edges(node, graph)))
		{
			const auto& neighbour = target(edge, graph);
			auto new_weight = weights[node] + graph[edge];
			if(weights[node] == max_weight)
			{
				new_weight = max_weight;
			}
			if(weights[neighbour] > new_weight)
			{
				std::cerr << "Problem with edge (" << node << ',' << neighbour << "): calculated: " << weights[neighbour];
				std::cerr << " new: " << new_weight << std::endl;
				ok = false;
			}
		}
	}
	return ok;
}
}
