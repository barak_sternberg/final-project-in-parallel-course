import random
import os
import progressbar

GSIZE = int(2 ** 17)
GRAPHS_DIR = r'c:\temp\graphs\\'
expectation = (GSIZE * 0.001)
variance = 1

def graph_gen(size=GSIZE):
    edges = []
    edges_dict = dict()
    progrbar = progressbar.ProgressBar()
    for cur_node in progrbar(xrange(size)):
        edges_cnt = int(random.normalvariate(expectation, variance))
        j = 0
        while j < edges_cnt:
            other_node = random.randint(0, size)
            # This check can be computationaly hard makes it slower alot.
            if cur_node in edges_dict.get(other_node, []) or other_node in edges_dict.get(cur_node, []):
                pass
            #    continue # already exists edge.
            else:
                cedges_arr = edges_dict.get(cur_node, list())
                cedges_arr.append(other_node)
                edges_dict[cur_node] = cedges_arr
                j += 1

    for node in edges_dict:
        for other_node in edges_dict[node]:
            edges.append((node+1, other_node+1))

    return edges

def write_graph_to_file(edges_arr, id_num):
    f = open(os.path.join(GRAPHS_DIR, 'graph%d.txt' % id_num), 'wb')
    for (a, b) in edges_arr:
        f.write('%d\t%d\r\n' % (a, b))
    f.close()

def gen_graphs(count=20):
    for i in xrange(count):
        print "Generating graph no.%d" % i
        graph = graph_gen()
        print "Writing graph to file %s\\graph%d.txt" % (GRAPHS_DIR, i)
        write_graph_to_file(graph, i)