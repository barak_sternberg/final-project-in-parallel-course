#pragma once

#include <atomic>
#include <cassert>

#include "cache_line.hpp"

namespace common
{

// Extrapulate memory order from ref counting example
template<class Integer=int>
struct alignas(cache_line_size) reference_counter
{
	std::atomic<Integer> ref_;

	reference_counter(const Integer& num):
		ref_(num)
	{
		assert(ref_.is_lock_free());
	}

	void increment() noexcept
	{
		ref_.fetch_add(1, std::memory_order_relaxed);
	}

	bool decrement() noexcept
	{
		return ref_.fetch_sub(1, std::memory_order_acq_rel) == 1;
	}

	bool empty() const noexcept
	{
		return ref_.load(std::memory_order_acq_rel) == 0;
	}
};
}
