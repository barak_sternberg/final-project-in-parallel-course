#pragma once

#include <mutex>

#include "common/cache_line.hpp"
#include "tasks_queue.hpp"

namespace queue
{
namespace lock
{

template<class PriorityType, class ValueType>
struct alignas(common::cache_line_size) queue
{
private:
	using queue_type = sequential::tasks_queue<PriorityType, ValueType>;

public:
	using element = typename queue_type::element;

private:
	queue_type tasks;
	std::mutex tasks_mutex;

public:

	queue(size_t /* threads num */, size_t task_count)
	{
		tasks.reserve(task_count);
	}

	auto top_pop()
	{
		return tasks.top_pop();
	}

	void push(element&& e)
	{
		tasks.push(std::move(e));
	}

	void push(const element& e)
	{
		tasks.push(e);
	}

	auto& mutex()
	{
		return tasks_mutex;
	}

	auto empty()
	{
		return tasks.empty();
	}
};

template<class Queue>
struct observer
{
	Queue& queue;
	using element = typename Queue::element;
	auto top_pop()
	{
		std::lock_guard<std::mutex> l(queue.mutex());
		return queue.top_pop();
	}

	template<class Element>
	void push(Element&& e)
	{
		std::lock_guard<std::mutex> l(queue.mutex());
		queue.push(std::forward<Element>(e));
	}


	void dispose(element&&) {}

	observer(Queue& queue_, size_t /*tid*/): queue(queue_) {}
};

}
}

