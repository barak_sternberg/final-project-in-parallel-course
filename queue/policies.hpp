#pragma once

#include <functional>
#include <utility>
#include <tuple>
#include <type_traits>

#include <boost/optional.hpp>

namespace queue
{
namespace policies
{

struct default_loader
{
	template<class Metadata>
	decltype(auto) inline operator()(const Metadata& m) const noexcept
	{
		return m.load();
	}
};

struct front_loader
{
	template<class Metadata>
	auto operator()(const Metadata& m) const noexcept
	{
		return m.front();
	}
};

struct back_loader
{
	template<class Metadata>
	auto operator()(const Metadata& m) const noexcept
	{
		return m.back();
	}
};

struct random
{
	template<class Observer>
	inline int operator()(Observer& observer) const noexcept
	{
		return observer.any();
	}
};

struct alloted
{
	template<class Observer>
	inline int operator()(Observer& observer) const noexcept
	{
		return observer.alloted();
	}
};

template<class Loader=default_loader, class Base=random>
struct valid
{
	const Base base{};

	template<class Observer>
	inline int operator()(Observer& observer) const noexcept
	{
		using metadata = std::decay_t<decltype(observer.metadata(std::declval<int>()))>;
		auto idx = base(observer);
		return metadata::is_valid(Loader{}(observer.metadata(idx)))
			? idx : observer.NOT_FOUND;
	}
};


template<size_t tries = 2, class Comparator=std::less<int>, class Loader=default_loader, class Base=random>
struct best_of
{
	const Base base{};

	template<class Observer>
	inline int operator()(Observer& observer) const noexcept
	{
		using metadata = std::decay_t<decltype(observer.metadata(std::declval<int>()))>;
		using value_type = std::decay_t<decltype(std::declval<Loader>()(std::declval<metadata&>()))>;
		Comparator cmp;
		boost::optional<std::pair<value_type, int>> m;
		Loader load;
		for(size_t i=0; i<tries; i++)
		{
			auto idx = base(observer);
			auto candidate = std::make_pair(load(observer.metadata(idx)), idx);
			if(!metadata::is_valid(candidate.first))
			{
				continue;
			}
			if(!m || cmp(candidate.first, m->first))
			{
				m = std::move(candidate);
			}
		}
		return m ? m->second : observer.NOT_FOUND;
	}
};

template<class Base, class Fallback>
struct fallback
{
	const Base base{};
	const Fallback fallback_{};

	template<class Observer>
	inline int operator()(Observer& observer) const noexcept
	{
		auto idx = base(observer);
		if(idx == observer.NOT_FOUND)
		{
			idx = fallback_(observer);
		}
		return idx;
	}
};
}
}
