#pragma once

#include <boost/heap/d_ary_heap.hpp>
#include <functional>
#include <type_traits>
#include "common/task.hpp"
#include "common/cache_line.hpp"

namespace queue
{
namespace sequential
{

namespace heap = boost::heap;
using task = common::task;

template<class PriorityType, class ValueType, bool IsMin>
struct priority_queue_base
{
private:
	static_assert(std::is_convertible<PriorityType, task::member_type>::value, "");
	static_assert(std::is_convertible<ValueType, task::member_type>::value, "");
	using element_type = task;
	using comparator = std::conditional_t<IsMin, std::greater<element_type>, std::less<element_type>>;
	static constexpr size_t arity = common::cache_line_size/sizeof(element_type);
public:
	using type = heap::d_ary_heap<element_type, heap::mutable_<false>, heap::arity<arity>, heap::compare<comparator>>;
};

template<class PriorityType, class ValueType, bool IsMin=true>
struct alignas(common::cache_line_size) tasks_queue: priority_queue_base<PriorityType, ValueType, IsMin>::type
{
private:
	using bundle = priority_queue_base<PriorityType, ValueType, IsMin>;
public:
	using base_type = typename bundle::type;
	using element = typename base_type::value_type;

	auto top_pop()
	{
		if(base_type::empty())
		{
			return element{};
		}
		else
		{
			auto ret = base_type::top();
			base_type::pop();
			return ret;
		}
	}

	void push(element&& e)
	{
		base_type::push(std::move(e));
	}

	void push(const element& e)
	{
		base_type::push(e);
	}
};

}
}
