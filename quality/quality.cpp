#include <iostream>
#include <fstream>
#include <cstring>
#include <thread>
#include <mutex>

#include "quality.hpp"
#include "common/policies.hpp"
#include "common/types.hpp"
#include "queue/multi_queue.hpp"

template<class Distribution>
void output_distribution(std::ostream& out, const Distribution& dist)
{
	for(const auto& p: dist)
	{
		out << p.first << ' ' << p.second << std::endl;
	}
}

int main(int argc, char* argv[])
{
	using namespace quality;
	int n0 = 100'000;
	int op_count = 1'000;
	int thread_count = 4;
	if(argc > 1 && !std::strcmp(argv[1], "-h"))
	{
		std::cerr << "Usage: " << argv[0] << " [thread-count=" << thread_count << "] ";
		std::cerr << "[op-count=" << op_count << "] [n0=" << n0 << "]" << std::endl;
		return 0;
	}
	if(argc > 1)
	{
		thread_count = std::atoi(argv[1]);
	}
	if(argc > 2)
	{
		op_count = std::atoi(argv[2]);
	}
	if(argc > 3)
	{
		n0 = std::atoi(argv[3]);
	}
	common::uniform_policy policy(0, 100'000'000);
	std::cout << "Starting computation: " << op_count << " ops" << std::endl;
	std::vector<std::thread> threads;
	threads.reserve(std::tuple_size<queue::all>::value);
	std::mutex printing_mutex;
	common::for_each(queue::all{}, [&] (auto e)
	{
		using types = typename decltype(e)::type;
		auto f = [&printing_mutex](auto&&... args)
		{
			auto dist = quality::calculate<types>(std::forward<decltype(args)>(args)...);
			constexpr auto name = common::type_name<types>::value;
			{
				std::lock_guard<std::mutex> l(printing_mutex);
				std::cout << "Outputing distribution for " << name << ":" << std::endl;
				output_distribution(std::cout, dist);
			}
		};
		threads.emplace_back(f, policy.duplicate(), policy.duplicate(), thread_count, n0, op_count);
	});
	for(auto& t: threads)
	{
		t.join();
	}
	return 0;
}
