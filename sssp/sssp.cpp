#include <iostream>
#include <vector>
#include <random>
#include <chrono>

#include "types.hpp"
#include "sssp.hpp"
#include "extract.hpp"
#include "common/types.hpp"
#include "queue/multi_queue.hpp"

auto init_graph()
{
	sssp::graph_type ret(5);
	using namespace boost;
	add_edge(1, 2, 4, ret);
	add_edge(1, 4, 2, ret);
	add_edge(2, 3, 5, ret);
	add_edge(3, 5, 2, ret);
	add_edge(3, 6, 6, ret);
	add_edge(4, 2, 1, ret);
	add_edge(4, 3, 8, ret);
	add_edge(4, 5, 10, ret);
	add_edge(5, 6, 3, ret);
	return ret;
}

const std::vector<int> expected = {common::task::max_priority, 0, 3, 8, 2, 10, 13};

int main(int argc, char* argv[])
{
	int thread_count=2;
	auto g = init_graph();
	if(argc > 1 && !std::strcmp(argv[1], "-h"))
	{
		std::cerr << "Usage: " << argv[0] << " [thread-count=" << thread_count << "] ";
		std::cerr << "[graph-file]" << std::endl;
		return 0;
	}
	if(argc > 1)
	{
		thread_count = std::atoi(argv[1]);
	}
	if(argc > 2)
	{
		std::fstream in(argv[2]);
		g = sssp::read_snap_unweighted<decltype(g)>(in, common::random_maker(1, 10));
	}
	if(num_vertices(g) == 0)
	{
		std::cerr << "Can't work on empty graph" << std::endl;
		return 1;
	}
	std::cout << "Starting computation: " << num_vertices(g) << " nodes" << std::endl;
	common::for_each(queue::all{}, [&](auto e)
	{
		using types = typename decltype(e)::type;
		auto res = sssp::calculate<types>(g, 1, thread_count);
		std::cout << common::type_name<types>::value << '\t' << res << std::endl;
	});
	return 0;
}
