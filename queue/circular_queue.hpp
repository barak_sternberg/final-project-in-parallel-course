#pragma once

#include <boost/circular_buffer.hpp>
#include <functional>
#include "common/task.hpp"
#include "common/cache_line.hpp"

namespace queue
{
namespace sequential
{

using task = common::task;

template<class PriorityType, class ValueType>
struct circular_buffer_base
{
private:
	static_assert(std::is_convertible<PriorityType, task::member_type>::value, "");
	static_assert(std::is_convertible<ValueType, task::member_type>::value, "");
	using element_type = task;
public:
	using type = boost::circular_buffer<element_type>;
};

template<class PriorityType, class ValueType>
struct alignas(common::cache_line_size) circular_queue: circular_buffer_base<PriorityType, ValueType>::type
{
	using base_type = typename circular_buffer_base<PriorityType, ValueType>::type;
	using element = typename base_type::value_type;

	auto top_pop()
	{
		if(base_type::empty())
		{
			return element{};
		}
		else
		{
			auto ret = base_type::front();
			base_type::pop_front();
			return ret;
		}
	}

	void push(element&& e)
	{
		assert(!base_type::full());
		base_type::push_back(std::move(e));
	}

	void push(const element& e)
	{
		assert(!base_type::full());
		base_type::push_back(e);
	}

	void reserve(size_t size)
	{
		base_type::set_capacity(size);
	}
};

}
}
