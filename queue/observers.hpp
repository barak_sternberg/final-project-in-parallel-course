#pragma once

#include "policies.hpp"
#include "bases.hpp"

namespace queue
{
namespace observers
{

using bases::observer;

template<class MultiQueueType>
using random = observer<
	MultiQueueType, policies::random, policies::random>;

template<class MultiQueueType>
using pop_valid = observer<
	MultiQueueType, policies::random, policies::valid<>>;

template<class MultiQueueType>
using pop_min = observer<
	MultiQueueType, policies::random, policies::best_of<>>;

template<class MultiQueueType>
using push_min = observer<
	MultiQueueType, policies::best_of<>, policies::random>;

template<class MultiQueueType>
using push_max = observer<
	MultiQueueType, policies::best_of<2, std::greater<int>>, policies::random>;

template<class MultiQueueType>
using pop_max = observer<
	MultiQueueType, policies::random, policies::best_of<2, std::greater<size_t>>>;

template<class MultiQueueType>
using pop_min_alloted = observer<
	MultiQueueType, policies::random, policies::fallback<
		policies::best_of<2, std::less<int>, policies::default_loader, policies::alloted>,
		policies::random
		>>;

template<class MultiQueueType>
using pop_random_alloted = observer<
	MultiQueueType, policies::random,
	policies::fallback<policies::alloted, policies::random>>;

template<class MultiQueueType>
using front_back = observer<
	MultiQueueType,
	policies::best_of<2, std::greater<int>, policies::back_loader>,
	//policies::random,
	policies::best_of<2, std::less<int>, policies::front_loader>>;

}
}
