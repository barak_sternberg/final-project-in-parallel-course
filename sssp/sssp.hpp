#pragma once

#include <atomic>
#include <cassert>

#include "common/pending_manager.hpp"
#include "common/distribute.hpp"
#include "verify.hpp"

namespace sssp
{

using common::pending_manager;

template<class Manager>
class worker
{
private:
	using graph_type = typename Manager::graph_type;
	using weights_type = typename Manager::weights_type;
	using observer_type = typename Manager::observer_type;
	using task_type = typename Manager::queue_type::element;
	const graph_type& graph_;
	weights_type& weights_;
	observer_type tasks_;
	pending_manager pending_;

public:
	worker(const graph_type& graph, weights_type& weights, observer_type&& tasks, pending_manager::counter_type& counter):
		graph_(graph), weights_(weights), tasks_(std::move(tasks)), pending_(counter)
	{}

	void operator()()
	{
		task_type current_task;
		while(true)
		{
			current_task = tasks_.top_pop();
			if(!current_task)
			{
				if(pending_.pend())
				{
					continue;
				}
				else
				{
					break;
				}
			}
			else 
			{
				pending_.resume();
			}
			consume_task(std::move(current_task));
		}
	}

private:
	void consume_task(task_type current_task)
	{
		const auto current = current_task.value();
		// Access is allowed to be relaxed, since the only needed guarantee
		// is that the loaded value was once stored.
		const auto current_weight = weights_[current].load(std::memory_order_relaxed);
		if(current_weight < current_task.priority())
		{ // stale task
			return;
		}
		auto neighbours = out_edges(current, graph_);
		for(auto it = neighbours.first;
				it != neighbours.second; ++it)
		{
			const auto edge = *it;
			const auto neighbour = target(edge, graph_);
			const auto new_weight = current_weight + graph_[edge];
			if(update_min(weights_[neighbour], new_weight))
			{
				tasks_.push(task_type{new_weight, neighbour});
			}
		}
		tasks_.dispose(std::move(current_task));
	}

	template<class AtomicType, class IntegerType>
	static bool update_min(AtomicType& value, const IntegerType new_value)
	{
		bool updated = false;
		// In this case a stale load is allowed, since it
		// will only cause the loop to iterate an additional extra time.
		auto temp = value.load(std::memory_order_relaxed);
		while(temp > new_value &&
				// Synchronization is only required for this atomic variable.
				!(updated = value.compare_exchange_weak(temp, new_value,
						std::memory_order_acq_rel)));
		return updated;
	}
};

template<class Types, class Graph, class AtomicType>
class manager
{
private:
	using atomic_integer_type = AtomicType;
	using raw_weight_type = typename Graph::edge_bundled;
	using weights_type = std::vector<atomic_integer_type>;
	using queue_type = typename Types::queue_type;
	using task_type = typename queue_type::element;
	using graph_type = Graph;
	using vertex_descriptor = typename Graph::vertex_descriptor;
	static constexpr auto max_weight = task_type::max_priority;
	using worker_type = worker<manager>;
	using observer_type = typename Types::observer_type;

	const graph_type& graph_;
	weights_type weights_;
	queue_type tasks_;
	pending_manager::counter_type working_counter_;
	double duration_;

	friend worker_type;

	static auto init_weights(size_t count)
	{
		weights_type weights(count);
		for(auto& e: weights)
		{
			// Store happens data race free
			e.store(max_weight, std::memory_order_relaxed);
		}
		return weights;
	}

	auto make_worker(size_t idx)
	{
		return worker_type(graph_, weights_, observer_type(tasks_, idx), working_counter_);
	}

	auto weights() const
	{
		std::vector<raw_weight_type> ret;
		ret.reserve(weights_.size());
		for(const auto& w: weights_)
		{
			ret.push_back(static_cast<raw_weight_type>(w));
		}
		return ret;
	}

public:
	/*
	 * @param graph
	 * @param source
	 * @param thread count - allowed to be 0, but notice that the queue must not know that.
	 * 0 is a sentinel value for own thread execution.
	 * @param args - arguments to be passed to the queue.
	 */
	template<class... Args>
	manager(const graph_type& graph, vertex_descriptor source, size_t thread_count,
			Args&&... args):
		graph_{graph}, weights_(init_weights(num_vertices(graph_))),
	  tasks_(std::max(thread_count, size_t{1}), num_vertices(graph)*2, std::forward<Args>(args)...),
		working_counter_(0), duration_(-1)
	{
		tasks_.push(task_type{0, source});
		// Store happens data race free
		weights_[source].store(0, std::memory_order_relaxed);
		auto factory = [this] (size_t i)
		{
			return make_worker(i);
		};
		duration_ = common::distribute(factory, thread_count);
		assert(verify(graph_, weights()));
	}

	auto duration() const
	{
		return duration_;
	}

};

// Extra arguments are passed to queue constructor
template<class Types, class Graph, class... Args>
auto calculate(const Graph& graph,
			typename Graph::vertex_descriptor source, size_t thread_count, Args&&... args)
{
	using integer_type = typename Graph::edge_bundled;
	static_assert(std::is_integral<integer_type>::value, "IntegerType must be an integer");
	using atomic_type = std::atomic_int_fast32_t;
	static_assert(std::is_convertible<integer_type, std::int_fast32_t>::value,
			"Must be able to store weight in atomic_type");
	assert(atomic_type{}.is_lock_free());
	static_assert(std::is_convertible<typename Graph::vertex_descriptor, int>::value,
			"Graph vertex_descriptor must be convertible to an int");
	manager<Types, Graph, atomic_type> manager(graph, source, thread_count, std::forward<Args>(args)...);
	return manager.duration();
}

}


