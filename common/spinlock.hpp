#pragma once

#include <atomic>

namespace common
{
struct spinlock
{
private:
	std::atomic_flag flag = ATOMIC_FLAG_INIT;
public:
	inline void lock() noexcept
	{
		while(flag.test_and_set(std::memory_order_acquire));
	}
	inline void unlock() noexcept
	{
		flag.clear(std::memory_order_release);
	}
	inline bool try_lock() noexcept
	{
		return !flag.test_and_set(std::memory_order_acquire);
	}
};
}
