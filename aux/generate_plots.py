#! /usr/bin/env python3
'''
SSSP results example:
"Starting computation: 3072627 nodes
classic	0.478379
non_empty	0.463801
stupid	0.470218
alloted	0.337805
stupid_alloted	0.31435
pop_max_length  0.539825
push_min_length	0.531951
circular	0.844494
approximate_max_element	0.642372
lock_queue	16.7918
worst	28.8273
"
'''
from __future__ import print_function
from matplotlib import pyplot as plt
import os
import re
import logging
import glob
import math
import contextlib

ALGS = ['classic', 'non_empty', 'stupid', 'alloted', 'stupid_alloted',
        'pop_max_length', 'push_min_length', 'circular', 'approximate_max_element',
        'lock_queue', 'worst']
FANCY_NAMES = {'stupid': 'naive', 'stupid_alloted': 'naive_alloted',
               'non_empty': 'cached'}
plt.style.use('seaborn-paper')
FONTSIZE = 'medium'

def get_core_num(fname):
    return int(re.search(r'(\d+)\.txt$', os.path.basename(fname)).group(1))

def get_times(res_dir='sssp_tests'):
    algs_dict = dict()
    for fname in glob.iglob(os.path.join(res_dir, '*.txt')):
        logging.info("Read file %s", fname)
        cores = get_core_num(fname)
        with open(fname, 'r', encoding='ascii') as atimes:
            lines = iter(atimes)
            next(lines)
            for alg, time in map(str.split, lines):
                if alg not in ALGS:
                    logging.warn("Found unknown alg: %s", alg)
                    continue
                algs_dict.setdefault(alg, dict())[cores] = float(time)
    return algs_dict

def get_quality(res_dir='quality_tests'):
    algs_dict = dict()
    template_line = 'Outputing distribution for (.*):'
    for fname in glob.iglob(os.path.join(res_dir, 'quality_cores*.txt')):
        logging.info("Read file %s", fname)
        cores = get_core_num(fname)
        cur_alg = None
        with open(fname, 'r', encoding = 'ascii') as f:
            lines = iter(f)
            next(lines)
            for line in lines:
                if line.startswith('Starting') or line == '':
                    continue
                m = re.match(template_line, line)
                if m:
                    cur_alg = m.group(1)
                    assert cur_alg in ALGS
                else:
                    assert cur_alg is not None
                    k, v = map(float, line.split())
                    record = algs_dict.setdefault(cur_alg, dict())
                    record.setdefault(cores, dict())[k] = v
    return algs_dict

def mean_std(distrib):
    moments = [0.0] * 3
    for value, amount in distrib.items():
        for i in range(len(moments)):
            moments[i] += amount * (value ** i)
    mean = moments[1]/moments[0]
    variance = (moments[0] * moments[2] - moments[1] ** 2) / moments[0] ** 2
    return mean, math.sqrt(variance)

def cummulative(distrib):
    n = len(distrib)
    records = list(distrib.items())
    records.sort()
    partial_sums = [0] * (n + 1)
    for i, v in zip(range(n - 1, -1, -1), reversed(records)):
        partial_sums[i] = partial_sums[i + 1] + v[1]
    total = partial_sums[0]
    return [(r[0], s/total) for r, s in zip(records, partial_sums)]

@contextlib.contextmanager
def make_graph(xlabel, ylabel, filename, **kwargs):
    fig = plt.figure(figsize=(9, 6))
    ax = fig.add_subplot(111)
    yield ax
    ax.legend(loc='best', fontsize=FONTSIZE, **kwargs.get('legend', dict()))
    ax.set_xlabel(xlabel, fontsize=FONTSIZE, **kwargs.get('xlabel', dict()))
    ax.set_ylabel(ylabel, fontsize=FONTSIZE, **kwargs.get('xlabel', dict()))
    plt.savefig(filename, bbox_inches='tight')

def lin_approx(lower, higher, v):
    x1, y1 = lower
    x2, y2 = higher

    c = (y2 - v) / (y2 - y1)

    weigheted = lambda a, b: a * c + (1 - c) * b

    assert abs(weigheted(y1, y2) - v) < 0.0001

    return weigheted(x1, x2)

def get_precentile(cum_dist, precentile=0.9):
    total = cum_dist[0][1]
    target_value = total * precentile
    idx, lower = next((i, v) for i, v in enumerate(cum_dist) if v[1] <= target_value)
    if lower[1] == target_value:
        return lower[0]
    higher = cum_dist[idx -1]
    assert higher[1] > target_value
    return lin_approx(lower, higher, target_value)


def draw_quality_graph():
    #TDL: for each algorithm draw its bar graph, compare various cores with one another algorithms as well.
    quality = get_quality()

    algs_mean_and_std = {alg: {core: mean_std(dist) for core,dist in
                               records.items()} for alg, records in
                         quality.items()}

    algs_cumulative = {alg: {core: cummulative(dist) for core,dist in
                               records.items()} for alg, records in
                         quality.items()}

    import json
    json.dump(algs_mean_and_std, open('values.json', 'w', encoding='ascii'),
              indent=2)

    def plot_mean(algs_, filename):
        global ALGS
        assert all(a in ALGS for a in algs_)
        with make_graph('cores', 'rank', filename, legend={'ncol': 2}) as ax:
            for alg in algs_:
               record = [(k, *v) for k,v in algs_mean_and_std[alg].items()]
               record.sort()
               x, y, e = zip(*record)
               ax.plot(x, y, label=FANCY_NAMES.get(alg, alg))

    def plot_precentile(algs_, filename):
        global ALGS
        assert all(a in ALGS for a in algs_)
        with make_graph('cores', 'rank', filename, legend={'ncol': 2}) as ax:
            for alg in algs_:
               record = [(k, get_precentile(v)) for k,v in algs_cumulative[alg].items()]
               record.sort()
               x, y = zip(*record)
               ax.plot(x, y, label=FANCY_NAMES.get(alg, alg))


    def plot_cumm(algs_, filename, core):
        global ALGS
        assert all(a in ALGS for a in algs_)
        with make_graph('cores', 'rank', filename) as ax:
            for alg in algs_:
               record = algs_cumulative[alg][core]
               x, y = zip(*record)
               ax.plot(x, y, label=FANCY_NAMES.get(alg, alg))

    interesting_algs = list(ALGS)
    interesting_algs.remove('circular')
    interesting_algs.remove('worst')
    interesting_algs.remove('lock_queue')
    interesting_algs.remove('non_empty')
    plot_cumm(interesting_algs, 'final/quality_cumm_all.png', 56)
    plot_mean(interesting_algs, 'final/quality_mean_all.png')
    plot_precentile(interesting_algs, 'final/quality_precentile_all.png')

def draw_sssp_graphs(graph):
    times = get_times('{}_tests'.format(graph))

    def draw_single_graph(algs_, filename):
        global ALGS
        assert all(a in ALGS for a in algs_)
        with make_graph('cores', 'time [sec]', filename) as ax:
            for alg in filter(times.__contains__, algs_):
                record = list(times[alg].items())
                record.sort()
                ax.plot(*zip(*record), label=FANCY_NAMES.get(alg, alg))

    draw_single_graph(times.keys(), 'final/{}_sssp_all_times.png'.format(graph))
    draw_single_graph(['worst', 'lock_queue', 'classic'],
                      'final/{}_sssp_bad.png'.format(graph))
    draw_single_graph(['push_min_length', 'pop_max_length', 'non_empty',
                       'circular',
                       'stupid', 'approximate_max_element', 'classic'],
                      'final/{}_sssp_ok.png'.format(graph))
    draw_single_graph(['alloted', 'stupid_alloted', 'classic'],
                      'final/{}_sssp_good.png'.format(graph))

def draw_throughput_graphs():
    times = get_times('throughput_tests')

    def draw_single_graph(algs_, filename):
        global ALGS
        assert all(a in ALGS for a in algs_)
        with make_graph('cores', 'throughput [ops/sec]', filename) as ax:
            for alg in algs_:
                record = list(times[alg].items())
                record.sort()
                ax.plot(*zip(*record), label=FANCY_NAMES.get(alg, alg))

    draw_single_graph(times.keys(), 'final/throughput_all.png')
    draw_single_graph(['push_min_length', 'pop_max_length', 'non_empty',
                       'approximate_max_element', 'stupid', 'classic',
                       'lock_queue'],
                      'final/throughput_ok.png')
    draw_single_graph(['alloted', 'stupid_alloted', 'circular', 'classic'],
                      'final/throughput_good.png')

def draw_compound_graphs():
    algs_ = ['alloted', 'stupid_alloted', 'classic']
    orkut = get_times('orkut_tests')
    roadnet = get_times('roadNet_tests')
    fig, (orkut_ax, roadnet_ax) = plt.subplots(ncols=2, figsize=(9,6))
    handles = dict()
    for ax, data, name in [(orkut_ax, orkut, 'orkut'), (roadnet_ax, roadnet,
                                                        'road-net')]:
        for alg in algs_:
            record = [(c, t) for c, t in data[alg].items()]
            record.sort()
            h, = ax.plot(*zip(*record))
            handles[(name, alg)] = h
    for alg in algs_:
        s = set(h.get_color() for (name, a), h in handles.items() if a == alg)
        assert len(s) == 1
    items = [(h, a) for (name, a), h in handles.items() if name == 'orkut']
    fig.legend(*zip(*items), loc='right', fontsize=FONTSIZE)
    for ax, name in [(orkut_ax, 'orkut'), (roadnet_ax, 'road-net')]:
        ax.set_xlabel('cores', fontsize=FONTSIZE)
        ax.set_ylabel('time [sec]', fontsize=FONTSIZE)
        ax.set_title(name, fontsize=FONTSIZE)
    plt.savefig('final/all_sssp_good.png', bbox_inches='tight')


if __name__ == '__main__':
    #logging.basicConfig(level=logging.INFO)
    draw_quality_graph()
    draw_sssp_graphs('orkut')
    draw_sssp_graphs('roadNet')
    draw_throughput_graphs()
    draw_compound_graphs()
