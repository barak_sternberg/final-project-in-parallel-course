#pragma once

#include <random>

namespace common
{

struct random_maker
{
private:
	std::uniform_int_distribution<> dist;
	std::minstd_rand prng;

	static auto seed()
	{
		std::random_device rng;
		return rng();
	}

	template<class Dist>
	random_maker(Dist&& dist_):
		dist(std::forward<Dist>(dist_)),
		prng(seed())
	{}
public:
	inline auto operator()()
	{
		return dist(prng);
	}

	random_maker(size_t bottom, size_t top):
		dist(bottom, top), prng(seed())
	{}

	auto duplicate() const
	{
		return random_maker(dist);
	}
};

}
