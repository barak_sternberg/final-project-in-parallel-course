#pragma once

#include <vector>
#include <cassert>
#include <unordered_map>
#include <set>
#include <map>
#include <algorithm>


#include "common/random_maker.hpp"

namespace quality
{

template<class Types, class FillPolicy, class UpdatePolicy>
class manager
{
private:
	using queue_type = typename Types::queue_type;
	using task_type = typename queue_type::element;
	using observer_type = typename Types::observer_type;

	queue_type tasks_;
	std::vector<observer_type> observers_;
	common::random_maker observer_index_;
	FillPolicy fill_policy_;
	UpdatePolicy update_policy_;
	std::unordered_map<int, int> distribution_;
	std::multiset<task_type> ranker_;

	auto& observer()
	{
		return observers_[observer_index_()];
	}

	void fill_queue(size_t n)
	{
		for(size_t i=0; i<n; i++)
		{
			auto task = fill_policy_();
			ranker_.insert(task);
			observer().push(std::move(task));
		}
	}

	void work(size_t operation_count)
	{
		// At this point the queue is pretty full, and wont ever be empty
		for(size_t i = 0; i<operation_count/2; i++)
		{
			auto task = remove();
			update_policy_(task);
			insert(std::move(task));
		}
	}

	void insert(task_type t)
	{
		ranker_.insert(t);
		observer().push(std::move(t));
	}

	auto remove()
	{
		task_type task;
		do
		{
			if(tasks_.empty())
			{
				return task;
			}
			task = observer().top_pop();
		} while(!task);
		auto it = ranker_.find(task);
		assert(it != ranker_.end());
		distribution_[std::distance(ranker_.begin(), it)]++;
		ranker_.erase(it);
		return task;
	}

	void init_observers(size_t thread_count)
	{
		observers_.reserve(thread_count);
		for(size_t i = 0; i < thread_count; i++)
		{
			observers_.emplace_back(tasks_, i);
		}
	}

public:
	/*
	 * @param graph
	 * @param source
	 * @param thread count - allowed to be 0, but notice that the queue must not know that.
	 * 0 is a sentinel value for own thread execution.
	 * @param args - arguments to be passed to the queue.
	 */
	template<class... Args>
	manager(size_t thread_count, size_t n0, size_t operation_count,
			FillPolicy&& fill_policy, UpdatePolicy&& update_policy,
			Args&&... args):
	  tasks_(thread_count, n0+operation_count, std::forward<Args>(args)...),
		observer_index_(0, thread_count-1),
		fill_policy_(std::move(fill_policy)), update_policy_(std::move(update_policy))
	{
		init_observers(thread_count);
		assert(operation_count % 2 == 0);
		auto reserve = std::min(n0*2, n0+operation_count);
		distribution_.reserve(reserve);
		fill_queue(n0);
		work(operation_count);
	}

	auto distibution() const
	{
		return std::map<int, int>(distribution_.begin(), distribution_.end());
	}


};

// Extra arguments are passed to queue constructor
template<class Types, class FillPolicy, class UpdatePolicy, class... Args>
auto calculate(
		FillPolicy fill,
		UpdatePolicy update,
		size_t thread_count,
		size_t n0, size_t operation_count,
		Args&&... args)
{
	manager<Types, FillPolicy, UpdatePolicy> manager(
			thread_count, 
			n0, operation_count,
			std::move(fill), std::move(update),
			std::forward<Args>(args)...);
	return manager.distibution();
}

}


